package com.devapp.sigsv.controller;

import javax.validation.Valid;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Producto;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.service.ProductoService;
import com.devapp.sigsv.util.AppUrl;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AppUrl.URL_PRODUCTO)
public class ProductoController extends AbstractController {

    private ProductoService productoService;

    @Autowired
	public void setProductoService(ProductoService productoService) {
		this.productoService = productoService;
    }

    @GetMapping(value = AppUrl.URL_LST_GET_PAGINADO)
    public Page<Producto> getLstProducto(Pageable pageable,
            @RequestParam(AppConstantes.PARAM_PATH_tipo) Integer tipo,
            @RequestParam(AppConstantes.PARAM_PATH_dato) String dato) {
		return productoService.lstProductoPaginado(pageable, tipo, dato);
    }

    @GetMapping(value = AppUrl.URL_DETALLE)
    public GenericResponse<Producto> getProducto(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id) 
            throws AppInternalException {
        GenericResponse<Producto> response = productoService.detalleProducto(id);
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }
    
    @PostMapping(value = AppUrl.URL_INSERTAR)
    public GenericResponse<Producto> saveProducto(@Valid @RequestBody Producto req) 
            throws AppInternalException {
        productoService.saveProducto(req);
		GenericResponse<Producto> response = new GenericResponse<>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
    }

    @PutMapping(value = AppUrl.URL_MODIFICAR)
    public GenericResponse<Producto> updateProducto(@Valid @RequestBody Producto req)
            throws AppInternalException {
        productoService.updateProducto(req);
        GenericResponse<Producto> response = new GenericResponse<>();
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }

    @GetMapping(value = AppUrl.URL_IND_ACTIVO)
    public GenericResponse<Producto> indActivoProducto(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id, @PathVariable (AppConstantes.PARAM_PATH_IND_ACTIVO) Boolean ind)
            throws AppInternalException {
		GenericResponse<Producto> response = productoService.indActivoProducto(id, ind);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
	}
    
}