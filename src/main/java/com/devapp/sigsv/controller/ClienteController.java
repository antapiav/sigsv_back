package com.devapp.sigsv.controller;

import javax.validation.Valid;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Cliente;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.service.ClienteService;
import com.devapp.sigsv.util.AppUrl;

import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AppUrl.URL_CLIENTE)
public class ClienteController extends AbstractController {

    private ClienteService clienteService;

    @Autowired
	public void setClienteService(ClienteService clienteService) {
		this.clienteService = clienteService;
    }

    @GetMapping(value = AppUrl.URL_LST_GET_PAGINADO)
    public Page<Cliente> getLstCliente(Pageable pageable,
            @RequestParam(AppConstantes.PARAM_PATH_tipo) Integer tipo,
            @RequestParam(AppConstantes.PARAM_PATH_dato) String dato) {
        return clienteService.lstClientePaginado(pageable, tipo, dato);
    }

    @GetMapping(value = AppUrl.URL_DETALLE)
    public GenericResponse<Cliente> getCliente(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id) 
            throws AppInternalException {
        GenericResponse<Cliente> response = clienteService.detalleCliente(id);
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }

    @PostMapping(value = AppUrl.URL_INSERTAR)
    public GenericResponse<Cliente> saveCliente(@Valid @RequestBody Cliente req) 
            throws AppInternalException {
        clienteService.saveCliente(req);
		GenericResponse<Cliente> response = new GenericResponse<>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
    }

    @PutMapping(value = AppUrl.URL_MODIFICAR)
    public GenericResponse<Cliente> updateCliente(@Valid @RequestBody Cliente req)
            throws AppInternalException {
        clienteService.updateCliente(req);
        GenericResponse<Cliente> response = new GenericResponse<>();
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }

    @GetMapping(value = AppUrl.URL_IND_ACTIVO)
    public GenericResponse<Cliente> indActivoCliente(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id, @PathVariable (AppConstantes.PARAM_PATH_IND_ACTIVO) Boolean ind)
            throws AppInternalException {
		GenericResponse<Cliente> response = clienteService.indActivoCliente(id, ind);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
	}
}