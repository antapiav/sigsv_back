package com.devapp.sigsv.controller;

import com.devapp.sigsv.util.AppUrl;

import javax.validation.Valid;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Proveedor;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.service.ProveedorService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AppUrl.URL_PROVEEDOR)
public class ProveedorController extends AbstractController {

    private ProveedorService proveedorService;

    @Autowired
	public void setProveedorService(ProveedorService proveedorService) {
		this.proveedorService = proveedorService;
    }

    @GetMapping(value = AppUrl.URL_LST_GET_PAGINADO)
    public Page<Proveedor> getLstProveedor(Pageable pageable,
            @RequestParam(AppConstantes.PARAM_PATH_tipo) Integer tipo,
            @RequestParam(AppConstantes.PARAM_PATH_dato) String dato) {
		return proveedorService.lstProveedorPaginado(pageable, tipo, dato);
    }

    @GetMapping(value = AppUrl.URL_DETALLE)
    public GenericResponse<Proveedor> getProveedor(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id) 
            throws AppInternalException {
        GenericResponse<Proveedor> response = proveedorService.detalleProveedor(id);
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }
    
    @PostMapping(value = AppUrl.URL_INSERTAR)
    public GenericResponse<Proveedor> saveProveedor(@Valid @RequestBody Proveedor req) 
            throws AppInternalException {
        proveedorService.saveProveedor(req);
		GenericResponse<Proveedor> response = new GenericResponse<>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
    }

    @PutMapping(value = AppUrl.URL_MODIFICAR)
    public GenericResponse<Proveedor> updateProveedor(@Valid @RequestBody Proveedor req)
            throws AppInternalException {
        proveedorService.updateProveedor(req);
        GenericResponse<Proveedor> response = new GenericResponse<>();
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }

    @GetMapping(value = AppUrl.URL_IND_ACTIVO)
    public GenericResponse<Proveedor> indActivoProveedor(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id, @PathVariable (AppConstantes.PARAM_PATH_IND_ACTIVO) Boolean ind)
            throws AppInternalException {
		GenericResponse<Proveedor> response = proveedorService.indActivoProveedor(id, ind);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
	}
    
}