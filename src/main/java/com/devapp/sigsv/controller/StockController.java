package com.devapp.sigsv.controller;

import javax.validation.Valid;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Stock;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.service.StockService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = AppUrl.URL_STOCK)
public class StockController extends AbstractController {

    private StockService stockService;

    @Autowired
	public void setStockService(StockService stockService) {
		this.stockService = stockService;
    }
    
    @GetMapping(value = AppUrl.URL_LST_GET_PAGINADO)
    public Page<Stock> getLstStock(Pageable pageable,
            @RequestParam(AppConstantes.PARAM_PATH_tipo) Integer tipo,
            @RequestParam(AppConstantes.PARAM_PATH_dato) String dato) {
		return stockService.lstStockPaginado(pageable, tipo, dato);
    }

    @GetMapping(value = AppUrl.URL_DETALLE)
    public GenericResponse<Stock> getStock(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id) 
            throws AppInternalException {
        GenericResponse<Stock> response = stockService.detalleStock(id);
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }
    
    @PostMapping(value = AppUrl.URL_INSERTAR)
    public GenericResponse<Stock> saveStock(@Valid @RequestBody Stock req) 
            throws AppInternalException {
        stockService.saveStock(req);
		GenericResponse<Stock> response = new GenericResponse<>();
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
    }

    @PutMapping(value = AppUrl.URL_MODIFICAR)
    public GenericResponse<Stock> updateStock(@Valid @RequestBody Stock req)
            throws AppInternalException {
        stockService.updateStock(req);
        GenericResponse<Stock> response = new GenericResponse<>();
        response.setCode(HttpStatus.OK.value());
        response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
        return response;
    }

    /*@GetMapping(value = AppUrl.URL_IND_ACTIVO)
    public GenericResponse<Stock> indActivoStock(@PathVariable(AppConstantes.PARAM_PATH_ID) Long id, @PathVariable (AppConstantes.PARAM_PATH_IND_ACTIVO) Boolean ind)
            throws AppInternalException {
		GenericResponse<Stock> response = stockService.indActivoStock(id, ind);
		response.setCode(HttpStatus.OK.value());
		response.setMessage(getValueMessage(AppMessages.GENERIC_SUCCESS));
		return response;
	}*/
}