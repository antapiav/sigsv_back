package com.devapp.sigsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SigsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(SigsvApplication.class, args);
	}

}
