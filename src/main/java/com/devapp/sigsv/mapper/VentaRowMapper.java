package com.devapp.sigsv.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.devapp.sigsv.model.bean.Cliente;
import com.devapp.sigsv.model.bean.Multitabla;
import com.devapp.sigsv.model.bean.Usuario;
import com.devapp.sigsv.model.bean.Venta;

import org.springframework.jdbc.core.RowMapper;

public class VentaRowMapper implements RowMapper<Venta> {

    @Override
    public Venta mapRow(ResultSet rs, int rowNum) throws SQLException {
        Venta venta = new Venta();
        Cliente cliente = new Cliente();
        Usuario usuario = new Usuario();
        Multitabla tipoComprobante = new Multitabla(); 
        Multitabla montoBolsa = new  Multitabla();

        venta.setIdVenta((Long) rs.getLong("id_venta"));
        venta.setNumComprobante((String) rs.getString("num_comproante"));
        venta.setFecha((Date) rs.getDate("fecha"));
        venta.setValorVenta((Double) rs.getDouble("valor_venta"));
        venta.setIgv((Double) rs.getDouble("igv"));
        venta.setImporteTotal((Double) rs.getDouble("importe_total"));
        venta.setDescuento((Double) rs.getDouble("descuento"));
        venta.setMontoPagado((Double) rs.getDouble("monto_pagado"));
        venta.setVuelto((Double) rs.getDouble("vuelto"));
        venta.setCantBolsa((Integer) rs.getInt("cant_bolsa"));
        venta.setIndActivo((Boolean) rs.getBoolean("ind_activo"));

        cliente.setIdCliente((Long) rs.getLong("id_cliente"));
        cliente.setDni((String) rs.getString("dni"));
        cliente.setNombre((String) rs.getString("nombre"));
        cliente.setApPaterno((String) rs.getString("ap_paterno"));
        cliente.setApMaterno((String) rs.getString("ap_materno"));
        cliente.setDireccion((String) rs.getString("direccion"));
        cliente.setIndActivo((Boolean) rs.getBoolean("ind_activo"));

        usuario.setIdUsuario((Long) rs.getLong("id_usuario"));
        usuario.setNombre((String) rs.getString("nombre"));
        usuario.setApPaterno((String) rs.getString("ap_paterno"));
        usuario.setApMaterno((String) rs.getString("ap_materno"));
        usuario.setDni((String) rs.getString("dni"));

        tipoComprobante.setIdCodigo((String) rs.getString("codigo"));
        tipoComprobante.setDescripcionValor((String) rs.getString("descripcion_valor"));

        montoBolsa.setIdCodigo((String) rs.getString("codigo"));
        montoBolsa.setDescripcionValor((String) rs.getString("descripcion_valor"));

        venta.setMontoBolsa(montoBolsa);
        venta.setTipoComprobante(tipoComprobante);
        venta.setUsuario(usuario);
        venta.setCliente(cliente);

        return venta;
    }
    
}