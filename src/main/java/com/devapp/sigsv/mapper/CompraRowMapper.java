package com.devapp.sigsv.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.devapp.sigsv.model.bean.Compra;
import com.devapp.sigsv.model.bean.Multitabla;
import com.devapp.sigsv.model.bean.Proveedor;
import com.devapp.sigsv.model.bean.ProveedorCategoria;

import org.springframework.jdbc.core.RowMapper;

public class CompraRowMapper implements RowMapper<Compra> {

    @Override
    public Compra mapRow(ResultSet rs, int rowNum) throws SQLException {
        Compra compra = new Compra();
        Proveedor proveedor = new Proveedor();
        ProveedorCategoria proveedorCategoria = new ProveedorCategoria();
        Multitabla tipoComprobante = new Multitabla();

        compra.setIdCompra((Long) rs.getLong("id_compra"));
        compra.setNumComprobante((String) rs.getString("num_comprobante"));
        compra.setFecha((Date) rs.getDate("fecha"));
        compra.setValorVenta((Double) rs.getDouble("valor_venta"));
        compra.setIgv((Double) rs.getDouble("igv"));
        compra.setImporteTotal((Double) rs.getDouble("importe_total"));
        compra.setIndActivo((Boolean) rs.getBoolean("ind_activo"));

        proveedor.setIdProveedor((Long) rs.getLong("id_proveedor"));
        proveedor.setNombre((String) rs.getString("nombre"));
        proveedor.setIndActivo((Boolean) rs.getBoolean("ind_activo"));

        proveedorCategoria.setIdProveedorCategoria((Long) rs.getLong("id_proveedor_categoria"));
        proveedorCategoria.setNombre((String) rs.getString("nombre"));

        proveedor.setProveedorcategoria(proveedorCategoria);

        compra.setProveedor(proveedor);

        tipoComprobante.setIdCodigo((String) rs.getString("codigo"));
        tipoComprobante.setDescripcionValor((String) rs.getString("descripcion_valor"));

        compra.setTipoComprobante(tipoComprobante);
        return compra;
    }
    
}