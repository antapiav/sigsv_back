package com.devapp.sigsv.service;

import java.util.List;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Pagination;
import com.devapp.sigsv.model.bean.Venta;
import com.devapp.sigsv.model.bean.response.GenericResponse;

public interface VentaService {
    GenericResponse<List<Venta>> lstVentaPaginado(Pagination pagination);
    GenericResponse<Venta> saveVenta(Venta req) throws AppInternalException;
    GenericResponse<Venta> updateVenta(Venta req) throws AppInternalException;
    GenericResponse<Venta> detalleVenta(Long id) throws AppInternalException;
    GenericResponse<Venta> indActivoVenta(Long id, Boolean ind) throws AppInternalException;
}