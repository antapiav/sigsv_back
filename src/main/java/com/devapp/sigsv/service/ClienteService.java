package com.devapp.sigsv.service;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Cliente;
import com.devapp.sigsv.model.bean.response.GenericResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClienteService {
    Page<Cliente> lstClientePaginado(Pageable pageable, Integer tipo, String dato);
    GenericResponse<Cliente> saveCliente(Cliente req) throws AppInternalException;
    GenericResponse<Cliente> updateCliente(Cliente req) throws AppInternalException;
    GenericResponse<Cliente> detalleCliente(Long id) throws AppInternalException;
    GenericResponse<Cliente> indActivoCliente(Long id, Boolean ind) throws AppInternalException;  
}