package com.devapp.sigsv.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.dao.VentaDao;
import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Pagination;
import com.devapp.sigsv.model.bean.Venta;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvVenta;
import com.devapp.sigsv.repository.SgvVentaRepository;
import com.devapp.sigsv.service.VentaService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("ventaServiceImpl")
@Transactional
public class VentaServiceImpl implements VentaService {

    @Autowired
	private VentaDao ventaDao;

    @Autowired
	@Qualifier("sgvVentaRepository")
    private SgvVentaRepository sgvVentaRepository;

    @Override
    public GenericResponse<List<Venta>> lstVentaPaginado(Pagination pagination) {
        return ventaDao.findVentaPaginado(pagination);
    }

    @Override
    public GenericResponse<Venta> saveVenta(Venta venta) throws AppInternalException {
        sgvVentaRepository.save(AppUtilConverter.convertVentaToSgvVenta(venta));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Venta> updateVenta(Venta venta) throws AppInternalException {
        boolean exist = sgvVentaRepository.findById(venta.getIdVenta()).map(mapper->{
            mapper.setNumComprobante(venta.getNumComprobante());
            mapper.setFecha(venta.getFecha());
            mapper.setValorVenta(venta.getValorVenta());
            mapper.setIgv(venta.getIgv());
            mapper.setImporteTotal(venta.getImporteTotal());
            mapper.setDescuento(venta.getDescuento());
            mapper.setMontoPagado(venta.getMontoPagado());
            mapper.setVuelto(venta.getVuelto());
            mapper.setCantBolsa(venta.getCantBolsa());
            mapper.setIndActivo(venta.getIndActivo());
            mapper.setSgvCliente(AppUtilConverter.convertClienteToSgvCliente(venta.getCliente()));
            mapper.setSgvUsuario(AppUtilConverter.convertUsuarioToSgvUsuario(venta.getUsuario()));
            mapper.setSgvTipoComprobante(AppUtilConverter.convertMultitablaToSgvMultitabla(venta.getTipoComprobante()));
            mapper.setSgvMontoBolsa(AppUtilConverter.convertMultitablaToSgvMultitabla(venta.getMontoBolsa()));
            return sgvVentaRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Venta> detalleVenta(Long id) throws AppInternalException {
        SgvVenta sgvVenta = sgvVentaRepository.findById(id).orElse(null);
        if(sgvVenta == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Venta> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvVentaToVenta(sgvVenta));
        return ress;
    }

    @Override
    public GenericResponse<Venta> indActivoVenta(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvVentaRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvVentaRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }
    
}