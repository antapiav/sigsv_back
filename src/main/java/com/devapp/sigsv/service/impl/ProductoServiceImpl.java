package com.devapp.sigsv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Producto;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvProducto;
import com.devapp.sigsv.repository.SgvProductoRepository;
import com.devapp.sigsv.service.ProductoService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("productoServiceImpl")
@Transactional
public class ProductoServiceImpl implements ProductoService {

    private static final Logger log = LoggerFactory.getLogger(AppUtilConverter.class);

    @Autowired
    @Qualifier("sgvProductoRepository")
    private SgvProductoRepository sgvProductoRepository;

    @Override
    public Page<Producto> lstProductoPaginado(Pageable pageable, Integer tipo, String dato) {
        List<Producto> lstProducto = new ArrayList<>();
        Producto producto = new Producto();
        Page<SgvProducto> pgSgvProducto = null;
        switch (tipo) {
            case 1:
                pgSgvProducto = sgvProductoRepository.findByCodigo(pageable, dato);
                break;
            case 2:
                pgSgvProducto = sgvProductoRepository.findByName(pageable, dato);
                break;
            default:
                pgSgvProducto = sgvProductoRepository.findAll(pageable);
        }

        for (SgvProducto sgvProducto : pgSgvProducto.getContent()) {
            producto = AppUtilConverter.convertSgvProductoToProducto(sgvProducto);
            lstProducto.add(producto);
        }
        Page<Producto> ress = new PageImpl<>(lstProducto, pgSgvProducto.getPageable(), lstProducto.size());
        return ress;
    }

    @Override
    public GenericResponse<Producto> saveProducto(Producto producto) throws AppInternalException {

        ObjectMapper obj = new ObjectMapper();
        try {
            log.info("XXX: "+obj.writeValueAsString(producto));
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sgvProductoRepository.save(AppUtilConverter.convertProductoToSgvProducto(producto));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Producto> updateProducto(Producto producto) throws AppInternalException {
        boolean exist = sgvProductoRepository.findById(producto.getIdProducto()).map(mapper->{
            mapper.setNombre(producto.getNombre());
            mapper.setDetalle(producto.getDetalle());
            mapper.setCodigo(producto.getCodigo());
            mapper.setPreCosto(producto.getPreCosto());
            mapper.setPreVenta(producto.getPreVenta());
            mapper.setUtilidad(producto.getUtilidad());
            mapper.setIndActivo(producto.getIndActivo());
            mapper.setSgvProductoCategoria(AppUtilConverter.convertProductoCategoriaToSgvProductoCategoria(producto.getProductoCategoria()));
            mapper.setSgvUnidad(AppUtilConverter.convertMultitablaToSgvMultitabla(producto.getUnidad()));
            return sgvProductoRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Producto> detalleProducto(Long id) throws AppInternalException {
        SgvProducto sgvProducto = sgvProductoRepository.findById(id).orElse(null);
        if(sgvProducto == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Producto> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvProductoToProducto(sgvProducto));
        return ress;
    }

    @Override
    public GenericResponse<Producto> indActivoProducto(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvProductoRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvProductoRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }
    
}