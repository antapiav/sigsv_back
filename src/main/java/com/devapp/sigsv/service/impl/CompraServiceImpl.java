package com.devapp.sigsv.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.dao.CompraDao;
import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Compra;
import com.devapp.sigsv.model.bean.Pagination;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvCompra;
import com.devapp.sigsv.repository.SgvCompraRepository;
import com.devapp.sigsv.service.CompraService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("compraServiceImpl")
@Transactional
public class CompraServiceImpl implements CompraService {

    @Autowired
	private CompraDao compraDao;

    @Autowired
	@Qualifier("sgvCompraRepository")
    private SgvCompraRepository sgvCompraRepository;

    @Override
    public GenericResponse<List<Compra>> lstCompraPaginado(Pagination pagination) {
        return compraDao.findCompraPaginado(pagination);
    }

    @Override
    public GenericResponse<Compra> saveCompra(Compra compra) throws AppInternalException {
        sgvCompraRepository.save(AppUtilConverter.convertCompraToSgvCompra(compra));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Compra> updateCompra(Compra compra) throws AppInternalException {
        boolean exist = sgvCompraRepository.findById(compra.getIdCompra()).map(mapper->{
            mapper.setNumComprobante(compra.getNumComprobante());
            mapper.setFecha(compra.getFecha());
            mapper.setValorVenta(compra.getValorVenta());
            mapper.setIgv(compra.getIgv());
            mapper.setImporteTotal(compra.getImporteTotal());
            mapper.setIndActivo(compra.getIndActivo());
            mapper.setSgvProveedor(AppUtilConverter.convertProveedorToSgvProveedor(compra.getProveedor()));
            mapper.setSgvUsuario(AppUtilConverter.convertUsuarioToSgvUsuario(compra.getUsuario()));
            mapper.setSgvTipoComprobante(AppUtilConverter.convertMultitablaToSgvMultitabla(compra.getTipoComprobante()));
            return sgvCompraRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Compra> detalleCompra(Long id) throws AppInternalException {
        SgvCompra sgvCompra = sgvCompraRepository.findById(id).orElse(null);
        if(sgvCompra == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Compra> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvCompraToCompra(sgvCompra));
        return ress;
    }

    @Override
    public GenericResponse<Compra> indActivoCompra(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvCompraRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvCompraRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }
    
}