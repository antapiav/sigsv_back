package com.devapp.sigsv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Cliente;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvCliente;
import com.devapp.sigsv.repository.SgvClienteRepository;
import com.devapp.sigsv.service.ClienteService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("clienteServiceImpl")
@Transactional
public class ClienteServiceImpl implements ClienteService {

    @Autowired
	@Qualifier("sgvClienteRepository")
    private SgvClienteRepository sgvClienteRepository;

    @Override
    public Page<Cliente> lstClientePaginado(Pageable pageable, Integer tipo, String dato) {
        List<Cliente> lstCliente = new ArrayList<>();
        Page<SgvCliente> pgSgvCliente = null;
        switch(tipo){
            case 1:
                pgSgvCliente = sgvClienteRepository.findByDni(pageable, dato);
                break;
            case 2:
                pgSgvCliente = sgvClienteRepository.findByName(pageable, dato);
                break;
            default:
                pgSgvCliente = sgvClienteRepository.findAll(pageable);
        
        }
    	for (SgvCliente sgvCliente : pgSgvCliente.getContent()) {
            lstCliente.add(AppUtilConverter.convertSgvClienteToCliente(sgvCliente));
        }	
        Page<Cliente> ress = new PageImpl<>(lstCliente, pgSgvCliente.getPageable(), lstCliente.size());  
        return ress;
    }

    @Override
    public GenericResponse<Cliente> saveCliente(Cliente cliente) throws AppInternalException {
        sgvClienteRepository.save(AppUtilConverter.convertClienteToSgvCliente(cliente));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Cliente> updateCliente(Cliente cliente) throws AppInternalException {
        boolean exist = sgvClienteRepository.findById(cliente.getIdCliente()).map(mapper->{
            mapper.setNombre(cliente.getNombre());
            mapper.setApPaterno(cliente.getApPaterno());
            mapper.setApMaterno(cliente.getApMaterno());
            mapper.setDni(cliente.getDni());
            mapper.setIndActivo(cliente.getIndActivo());
            mapper.setDireccion(cliente.getDireccion());
            mapper.setContacto(cliente.getContacto());
            mapper.setTelefono(cliente.getTelefono());
            mapper.setEmail(cliente.getEmail());
            return sgvClienteRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Cliente> detalleCliente(Long id) throws AppInternalException {
        SgvCliente sgvCliente = sgvClienteRepository.findById(id).orElse(null);
        if(sgvCliente == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Cliente> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvClienteToCliente(sgvCliente));
        return ress;
    }

    @Override
    public GenericResponse<Cliente> indActivoCliente(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvClienteRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvClienteRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }
    
}