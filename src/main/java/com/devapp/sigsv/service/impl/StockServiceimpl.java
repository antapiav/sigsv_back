package com.devapp.sigsv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Stock;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvStock;
import com.devapp.sigsv.repository.SgvStockRepository;
import com.devapp.sigsv.service.StockService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("stockServiceImpl")
@Transactional
public class StockServiceimpl implements StockService {

    @Autowired
	@Qualifier("sgvStockRepository")
    private SgvStockRepository sgvStockRepository;

    @Override
    public Page<Stock> lstStockPaginado(Pageable pageable, Integer tipo, String dato) {
        List<Stock> lstStock = new ArrayList<>();
        Stock stock = new Stock();
        Page<SgvStock> pgSgvStock = null;

        switch(tipo){
            case 1:
            pgSgvStock = sgvStockRepository.findByProductoCodigo(pageable, dato);
                break;
            case 2:
            pgSgvStock = sgvStockRepository.findByProductoNombre(pageable, dato);
                break;
            default:
            pgSgvStock = sgvStockRepository.findAll(pageable);
        }

        for (SgvStock sgvStock : pgSgvStock.getContent()) {
            stock = AppUtilConverter.convertSgvStockToStock(sgvStock);
            lstStock.add(stock);
        }	
        Page<Stock> ress = new PageImpl<>(lstStock, pgSgvStock.getPageable(), lstStock.size());  
        return ress;
    }

    @Override
    public GenericResponse<Stock> saveStock(Stock stock) throws AppInternalException {
        sgvStockRepository.save(AppUtilConverter.convertStockToSgvStock(stock));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Stock> updateStock(Stock stock) throws AppInternalException {
        boolean exist = sgvStockRepository.findById(stock.getIdStock()).map(mapper->{
            mapper.setStockGeneral(stock.getStockGeneral());
            mapper.setSgvProducto(AppUtilConverter.convertProductoToSgvProducto(stock.getProducto()));
            return sgvStockRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Stock> detalleStock(Long id) throws AppInternalException {
        SgvStock sgvStock = sgvStockRepository.findById(id).orElse(null);
        if(sgvStock == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Stock> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvStockToStock(sgvStock));
        return ress;
    }

    /*@Override
    public GenericResponse<Stock> indActivoStock(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvStockRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvStockRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }*/
    
}