package com.devapp.sigsv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Proveedor;
import com.devapp.sigsv.model.bean.response.GenericResponse;
import com.devapp.sigsv.model.entity.SgvProveedor;
import com.devapp.sigsv.repository.SgvProveedorRepository;
import com.devapp.sigsv.service.ProveedorService;
import com.devapp.sigsv.util.AppConstantes;
import com.devapp.sigsv.util.AppMessages;
import com.devapp.sigsv.util.AppUtilConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service("proveedorServiceImpl")
@Transactional
public class ProveedorServiceImpl implements ProveedorService {

    @Autowired
	@Qualifier("sgvProveedorRepository")
    private SgvProveedorRepository sgvProveedorRepository;

    @Override
    public Page<Proveedor> lstProveedorPaginado(Pageable pageable, Integer tipo, String dato) {
        List<Proveedor> lstProveedor = new ArrayList<>();
        Proveedor proveedor = new Proveedor();
        Page<SgvProveedor> pgSgvProveedor = null;
        switch(tipo){
            case 1:
                pgSgvProveedor = sgvProveedorRepository.findByCodigo(pageable, dato);
                break;
            case 2:
                pgSgvProveedor = sgvProveedorRepository.findByName(pageable, dato);
                break;
            default:
                pgSgvProveedor = sgvProveedorRepository.findAll(pageable);
        }

    	for (SgvProveedor sgvProveedor : pgSgvProveedor.getContent()) {
            proveedor = AppUtilConverter.convertSgvProveedorToProveedor(sgvProveedor);
            lstProveedor.add(proveedor);
        }	
        Page<Proveedor> ress = new PageImpl<>(lstProveedor, pgSgvProveedor.getPageable(), lstProveedor.size());  
        return ress;
    }

    @Override
    public GenericResponse<Proveedor> saveProveedor(Proveedor proveedor) throws AppInternalException {
        sgvProveedorRepository.save(AppUtilConverter.convertProveedorToSgvProveedor(proveedor));
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Proveedor> updateProveedor(Proveedor proveedor) throws AppInternalException {
        boolean exist = sgvProveedorRepository.findById(proveedor.getIdProveedor()).map(mapper->{
            mapper.setNombre(proveedor.getNombre());
            mapper.setNumDocumento(proveedor.getNumDocumento());
            mapper.setDireccion(proveedor.getDireccion());
            mapper.setContacto(proveedor.getContacto());
            mapper.setIndActivo(proveedor.getIndActivo());
            mapper.setTelefono(proveedor.getTelefono());
            mapper.setEmail(proveedor.getEmail());
            mapper.setSgvProveedorcategoria(AppUtilConverter.convertProveedorCategoriaToSgvProveedorCategoria(proveedor.getProveedorcategoria()));
            mapper.setSgvTipoDocumento(AppUtilConverter.convertMultitablaToSgvMultitabla(proveedor.getTipoDocumento()));
            return sgvProveedorRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }

    @Override
    public GenericResponse<Proveedor> detalleProveedor(Long id) throws AppInternalException {
        SgvProveedor sgvProveedor = sgvProveedorRepository.findById(id).orElse(null);
        if(sgvProveedor == null){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY, AppConstantes.IS_MESSAGE_KEY);
        }
        GenericResponse<Proveedor> ress = new GenericResponse<>();
        ress.setBody(AppUtilConverter.convertSgvProveedorToProveedor(sgvProveedor));
        return ress;
    }

    @Override
    public GenericResponse<Proveedor> indActivoProveedor(Long id, Boolean ind) throws AppInternalException {
        boolean exist = sgvProveedorRepository.findById(id).map(mapper->{
            mapper.setIndActivo(ind);
            return sgvProveedorRepository.save(mapper);
        }).isPresent();
        if(!exist){
            throw new AppInternalException(HttpStatus.NOT_FOUND, AppMessages.GENERIC_GET_UNIQUE_EMPTY);
        }
        return new GenericResponse<>();
    }
    
}