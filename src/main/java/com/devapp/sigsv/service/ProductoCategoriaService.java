package com.devapp.sigsv.service;

import java.util.List;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.ProductoCategoria;
import com.devapp.sigsv.model.bean.response.GenericResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductoCategoriaService {
    Page<ProductoCategoria> lstProductoCategoriaPaginado(Pageable pageable, Integer tipo, String dato);
    GenericResponse<List<ProductoCategoria>> lstProductoCategoria()  throws AppInternalException;
    GenericResponse<ProductoCategoria> saveProductoCategoria(ProductoCategoria productoCategoria) throws AppInternalException;
    GenericResponse<ProductoCategoria> updateProductoCategoria(ProductoCategoria productoCategoria) throws AppInternalException;
    GenericResponse<ProductoCategoria> detalleProductoCategoria(Long id) throws AppInternalException;
    GenericResponse<ProductoCategoria> indActivoProductoCategoria(Long id, Boolean ind) throws AppInternalException;
}