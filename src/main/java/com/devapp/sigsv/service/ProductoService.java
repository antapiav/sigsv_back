package com.devapp.sigsv.service;

import com.devapp.sigsv.exception.AppInternalException;
import com.devapp.sigsv.model.bean.Producto;
import com.devapp.sigsv.model.bean.response.GenericResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductoService {
    Page<Producto> lstProductoPaginado(Pageable pageable, Integer tipo, String dato);
    GenericResponse<Producto> saveProducto(Producto producto) throws AppInternalException;
    GenericResponse<Producto> updateProducto(Producto producto) throws AppInternalException;
    GenericResponse<Producto> detalleProducto(Long id) throws AppInternalException;
    GenericResponse<Producto> indActivoProducto(Long id, Boolean ind) throws AppInternalException;
}