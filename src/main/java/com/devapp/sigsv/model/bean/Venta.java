package com.devapp.sigsv.model.bean;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Venta implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long idVenta;

    private String numComprobante;

    private Date fecha;

    private Double valorVenta;

    private Double igv;

    private Double importeTotal;

    private Double descuento;

    private Double montoPagado;

    private Double vuelto;

    private Integer cantBolsa;

    private Boolean indActivo;

    private Cliente cliente;

    private Usuario usuario;

    private Multitabla tipoComprobante;

    private Multitabla montoBolsa;
}