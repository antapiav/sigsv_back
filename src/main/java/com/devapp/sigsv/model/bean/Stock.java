package com.devapp.sigsv.model.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class Stock implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long idStock;

    private double stockOperacional;

    private double stockGeneral;

    private Producto producto;
    
}