package com.devapp.sigsv.model.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class Cliente implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCliente;

    private String dni;
    
    private String nombre;
    
    private String apPaterno;
    
    private String apMaterno;
    
    private String direccion;
    
    private String contacto;
    
    private String telefono;
    
    private String email;

    private Boolean indActivo;
}