package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvProveedor;
import com.devapp.sigsv.util.AppConstantes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("sgvProveedorRepository")
public interface SgvProveedorRepository extends JpaRepository<SgvProveedor, Long> {
    @Query("SELECT p FROM SgvProveedor p "+
    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.numDocumento)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvProveedor> findByCodigo(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
    @Query("SELECT p FROM SgvProveedor p "+
    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.nombre)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvProveedor> findByName(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
}