package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvMultitabla;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SgvMultitablaRepository extends JpaRepository<SgvMultitabla, Long> {
    
}