package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvDetalleVenta;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SgvDetalleVentaRepository extends JpaRepository<SgvDetalleVenta, Long> {
    
}