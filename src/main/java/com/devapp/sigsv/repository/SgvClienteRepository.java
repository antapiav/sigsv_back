package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvCliente;
import com.devapp.sigsv.util.AppConstantes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("sgvClienteRepository")
public interface SgvClienteRepository extends JpaRepository<SgvCliente, Long> {
    @Query("SELECT p FROM SgvCliente p WHERE p.dni LIKE concat('%', :dato,'%')")
    Page<SgvCliente> findByDni(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
    
    @Query("SELECT p FROM SgvCliente p "+
	    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.nombre, p.apPaterno, p.apMaterno)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
	    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvCliente> findByName(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
}