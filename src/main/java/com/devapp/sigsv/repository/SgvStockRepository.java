package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvProducto;
import com.devapp.sigsv.model.entity.SgvStock;
import com.devapp.sigsv.util.AppConstantes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("sgvStockRepository")
public interface SgvStockRepository extends JpaRepository<SgvStock, Long> {
    @Query("SELECT s FROM SgvStock s"+
        " INNER JOIN s.sgvProducto p"+
        " ON s.sgvProducto.idProducto = p.idProducto"+
        " WHERE p.idProducto.codigo LIKE concat('%', :dato,'%')"+
        " and p.indActivo = true")
    Page<SgvStock> findByProductoCodigo(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
    
    @Query("SELECT s FROM SgvStock s"+
        " INNER JOIN s.sgvProducto p"+
        " ON s.sgvProducto.idProducto = p.idProducto"+
        " WHERE TRANSLATE(REPLACE(UPPER(concat(p.nombre)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
        " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
        " and p.indActivo = true")
    Page<SgvStock> findByProductoNombre(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);

}