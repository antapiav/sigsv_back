package com.devapp.sigsv.repository;

import com.devapp.sigsv.model.entity.SgvProducto;
import com.devapp.sigsv.util.AppConstantes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("sgvProductoRepository")
public interface SgvProductoRepository extends JpaRepository<SgvProducto, Long> {
    @Query("SELECT p FROM SgvProducto p "+
    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.codigo)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvProducto> findByCodigo(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
    @Query("SELECT p FROM SgvProducto p "+
    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.nombre)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvProducto> findByName(Pageable pageable, @Param(AppConstantes.PARAM_PATH_dato) String dato);
}