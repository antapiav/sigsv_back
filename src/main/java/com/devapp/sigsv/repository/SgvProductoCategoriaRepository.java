package com.devapp.sigsv.repository;

import java.util.List;

import com.devapp.sigsv.model.entity.SgvProductoCategoria;
import com.devapp.sigsv.util.AppConstantes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository("sgvProductoCategoriaRepository")
public interface SgvProductoCategoriaRepository extends JpaRepository<SgvProductoCategoria, Long> {
    @Query("SELECT p FROM SgvProductoCategoria p "+
	    " WHERE TRANSLATE(REPLACE(UPPER(concat(p.nombre)), ' ', ''), 'ÁÉÍÓÚ','AEIOU')"+
	    " LIKE TRANSLATE(REPLACE(UPPER(concat('%', :dato,'%')), ' ', ''), 'ÁÉÍÓÚ','AEIOU')")
    Page<SgvProductoCategoria> findByName(Pageable pageable,  @Param(AppConstantes.PARAM_PATH_dato) String dato);
    List<SgvProductoCategoria> findAllByOrderByIdProductoCategoriaAsc();
}