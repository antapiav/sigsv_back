package com.devapp.sigsv.util;

public interface AppConstantes {

    String NOT_FOUND_KEY = "notFoundKey";
    
    String PARAM_PATH_ID = "id";
    String PARAM_PATH_IND_ACTIVO = "ind";

    String PARAM_PATH_tipo = "tipo";
    String PARAM_PATH_dato = "dato";

    Boolean IS_MESSAGE_KEY = true;
	Boolean IS_NOT_MESSAGE_KEY = false;

}
