package com.devapp.sigsv.util;

public interface AppMessages {
    
    String GENERIC_SUCCESS = "message.generic.success";
	String GENERIC_GET_UNIQUE_EMPTY = "message.generic.getUnique.empty";
}