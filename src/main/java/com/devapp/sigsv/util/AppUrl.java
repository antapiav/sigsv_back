package com.devapp.sigsv.util;

public interface AppUrl {

    String ALL = "/**";
	String ROOT = "/";
    String PATTERN_ACTUATOR = "/actuator/**";
    
    //path controller
    String URL_USUARIO = "/usuario";
    String URL_CLIENTE = "/cliente";
    String URL_COMPRA = "/compra";
    String URL_DETALLE_COMPRA = "/detalle_compra";
    String URL_DETALLE_COMPRA_TEMP = "/detalle_compra_temp";
    String URL_DETALLE_VENTA = "/detalle_venta";
    String URL_DETALLE_VENTA_TEMP = "/detalle_venta_temp";
    String URL_PRODUCTO = "/producto";
    String URL_PRODUCTO_CATEGORIA = "/producto_categoria";
    String URL_PROVEEDOR = "/proveedor";
    String URL_PROVEEDOR_CATEGORIA = "/proveedor_categoria";
    String URL_STOCK = "/stock";
    String URL_VENTA = "/venta";

    //path variables url tipo /{id}/
    String API_ID = "/{id}";
    String API_IND_ACTIVO = "/{ind}";

    //path metodos de clase
    String URL_LST_GET_PAGINADO = "/paginado";
    String URL_LST_GET = "/lst";
    String URL_INSERTAR = "/insertar";
    String URL_MODIFICAR = "/modificar";
    String URL_ELIMINAR = "/eliminar" + API_ID;
    String URL_IND_ACTIVO = "/ind_activo" + API_ID + API_IND_ACTIVO;
    String URL_DETALLE = "/detalle" + API_ID;
}